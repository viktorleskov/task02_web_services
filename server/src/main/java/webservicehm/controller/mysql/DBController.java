package webservicehm.controller.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import webservicehm.controller.mysql.core.DBClient;
import webservicehm.controller.mysql.core.DBQueryExecutor;
import webservicehm.controller.mysql.utils.Entity;
import webservicehm.controller.mysql.utils.Operation;
import webservicehm.controller.mysql.utils.QueryBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DBController {
    private static Logger LOG = LogManager.getLogger(DBController.class.getName());
    private WebElement kek;

    public static ResultSet select(Entity tableName, String statement) throws SQLException {
        return DBQueryExecutor.executeSelect(QueryBuilder.buildQuery(Operation.SELECT, tableName, statement));
    }

    public static void insert(Entity tableName, String fields, String values) throws SQLException {
        LOG.debug("Database controller trying to do INSERT.");
        DBClient.setForeignKeyCheck(false);
        DBQueryExecutor.execute(QueryBuilder.buildQuery(Operation.INSERT, tableName, fields, values));
        DBClient.setForeignKeyCheck(true);
    }

    public static void delete(Entity tableName, String statement) throws SQLException {
        LOG.debug("Database controller trying to do DELETE.");
        DBClient.setForeignKeyCheck(false);
        DBQueryExecutor.execute(QueryBuilder.buildQuery(Operation.DELETE, tableName, statement));
        DBClient.setForeignKeyCheck(true);
    }

    public static ResultSet update(Entity tableName, String field, String newValue, String statement) throws SQLException {
        LOG.debug("Database controller trying to do UPDATE.");
        DBClient.setForeignKeyCheck(false);
        DBQueryExecutor.execute(QueryBuilder.buildQuery(Operation.UPDATE, tableName, field, newValue, statement));
        DBClient.setForeignKeyCheck(true);
        return select(tableName,statement);
    }


}
