package webservicehm.controller.soap.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import webservicehm.controller.mysql.DBController;
import webservicehm.controller.mysql.utils.Entity;
import webservicehm.controller.rest.utils.ResultSetParser;
import webservicehm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService()
@RequestMapping("user")
@Service
@Api(value = "user",tags={"User-soap"})
public class UserSoapController {
    @ApiOperation(value = "get soap operation")
    @GetMapping(value = "/get")
    @WebMethod
    public @ResponseBody User getUser(String email) throws SQLException, IllegalAccessException, InstantiationException {
        return (User) ResultSetParser.parseResultSet(DBController.select(Entity.USER, "email='" + email + "'"), User.class);
    }

}
