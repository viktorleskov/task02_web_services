package webservicehm.configuration.mysqlconfig;

public enum Property {
    DB_USERNAME("user_name"), DB_USER_PASSWORD("user_password"),
    DB_NAME("db_name"), DB_HOST("db_host"),
    DB_PORT("db_port"), JDBC_URL("jdbc_url");

    Property(String propName) {
        this.propName = propName;
    }

    private String propName;

    public String prop() {
        return propName;
    }
}
