package webclient.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class HttpCrudClient {

    public static HttpResponse executeGet(String requestMessage) {
        org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        HttpGet request = new HttpGet(requestMessage);
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static void executeDelete(String requestMessage) {
        org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
        HttpDelete request = new HttpDelete(requestMessage);
        try {
            client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
