package webservicehm.controller.rest.core;

import com.google.gson.Gson;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import webservicehm.controller.mysql.DBController;
import webservicehm.controller.mysql.utils.Entity;
import webservicehm.controller.rest.utils.ResultSetParser;
import webservicehm.dto.User;

import java.sql.SQLException;

@Service
@RequestMapping("api/user")
@Api(value = "User", tags = {"User"})
public class UserController {
    @ApiOperation(value = "Get user by email", response = User.class, tags = "User")
    @GetMapping(value = "/get/{email}")
    public @ResponseBody
    User getUser(@PathVariable String email) throws SQLException, InstantiationException, IllegalAccessException {
        return (User) ResultSetParser.parseResultSet(DBController.select(Entity.USER, "email='" + email + ".com'"), User.class);
    }

    @ApiOperation(value = "Delete user by email", response = Iterable.class, tags = "User")
    @DeleteMapping(value = "/delete/{email}")
    public void deleteUser(@PathVariable String email) throws SQLException, InstantiationException, IllegalAccessException {
        DBController.delete(Entity.USER, "email='" + email + ".com'");
    }

    @ApiOperation(value = "Put user(only change name)", response = User.class, tags = "User")
    @PutMapping(value = "/put/{email},{last_name}")
    public @ResponseBody
    User putUser(@PathVariable("email") String email, @PathVariable("last_name") String last_name) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        return (User) ResultSetParser.parseResultSet(DBController.update(Entity.USER, "last_name", last_name, "email='" + email + "'"), User.class);
    }

    @ApiOperation(value = "Post new User", response = User.class, tags = "User")
    @PostMapping(value = "/post")
    public void postUser(@ApiParam(required = true, value = "{\n" +
            "\"email\": \"viktorius488@gmail.com\",\n" +
            "\"first_name\": \"Viktor\",\n" +
            "\"last_name\": \"Leskov\",\n" +
            "\"password\": \"5645\"\n" +
            "}", name = "User json")
                         @RequestBody String body) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.insert(Entity.USER, "email, first_name, last_name, password", new Gson().fromJson(body, User.class).toString());
    }
}
