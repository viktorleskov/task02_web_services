package webservicehm.controller.mysql.utils;

public enum Operation {
    SELECT("SELECT"), INSERT("INSERT"), DELETE("DELETE"), UPDATE("UPDATE");
    private String operation;

    Operation(String str) {
        operation = str;
    }

    public String getOperation() {
        return operation;
    }
}
