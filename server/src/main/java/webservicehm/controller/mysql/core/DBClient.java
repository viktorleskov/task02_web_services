package webservicehm.controller.mysql.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import webservicehm.configuration.mysqlconfig.DBProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

public class DBClient {
    private static Logger LOG = LogManager.getLogger(DBClient.class);
    private static DBProperties props = new DBProperties();
    private static Connection connection;

    private DBClient() {
    }

    static Connection getConnection() {
        return Optional.ofNullable(connection).isPresent() ?
                /*if present*/      connection :
                /*if NOT present*/  initializeConnection();
    }

    private static Connection initializeConnection() {
        synchronized (DBClient.class) {
            try {
                LOG.debug("Connecting to\'" + props.getDbName() + "\'using user_name: \'" + props.getDbUser()
                        + "\' and password: \'" + props.getDbPass() + "\' ...");
                connection = DriverManager.getConnection(props.getJDBCUrl(), props.getDbUser(), props.getDbPass());
                LOG.debug("Connected to \'" + props.getDbName() + "\' database successfully.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    public static void shutdown() {
        Optional.ofNullable(connection).ifPresent(thenExecute -> {
            try {
                connection.close();
                connection = null;
                LOG.debug("Connection to DataBase was closed!");
            } catch (SQLException e) {
                LOG.error("Error. Can`t close Connection.");
            }
        });
    }

    public static void setForeignKeyCheck(Boolean state) throws SQLException {
        if (state) {
            LOG.debug("Enable foreign key checks.");
            DBQueryExecutor.execute("SET FOREIGN_KEY_CHECKS=1");
        } else {
            LOG.debug("Disable foreign key checks.");
            DBQueryExecutor.execute("SET FOREIGN_KEY_CHECKS=0");
        }
    }

}
