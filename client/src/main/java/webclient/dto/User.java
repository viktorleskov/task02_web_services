package webclient.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@ApiModel(value = "User")
@XmlType(name = "User", propOrder = {
        "email",
        "first_name",
        "last_name",
        "password"
})
public class User implements Serializable {

    @XmlElement(required = true)
    @NotNull
    @NotEmpty
    @Column(name = "email")
    private String email;

    @XmlElement(required = true)
    @NotNull
    @NotEmpty
    @Column(name = "first_name")
    private String first_name;

    @XmlElement(required = true)
    @NotNull
    @NotEmpty
    @Column(name = "last_name")
    private String last_name;

    @XmlElement(required = true)
    @NotNull
    @NotEmpty
    @Column(name = "password")
    private String password;

    public User(String email, String first_name, String last_name, String password) {
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "'" + email + "',"
                + "'" + first_name + "',"
                + "'" + last_name + "',"
                + "'" + password + "'";
    }
}
