package webservicehm.controller.mysql.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public abstract class QueryBuilder {
    private static Logger LOG = LogManager.getLogger(QueryBuilder.class.getName());
    private static String BASE_SELECT = "SELECT * FROM %s where %s;"; //table, field, value
    private static String BASE_INSERT = "INSERT into %s(%s) values (%s);"; //table, fields name with comma, value with comma
    private static String BASE_DELETE = "DELETE FROM %s where %s;"; //table, field, value
    private static String BASE_UPDATE = "UPDATE %s SET %s = '%s' where %s;"; //table, field, new value, field, actual value

    public static String buildQuery(Operation operation, Entity tableName, String... args) {
        LOG.debug("start building query.");
        List arguments = Arrays.asList(args);
        String query = "";
        if (operation == Operation.SELECT) {
            LOG.debug("Operation 'SELECT' was selected.");
            query = String.format(BASE_SELECT, tableName.getValue(), arguments.get(0));
            LOG.debug("Query: " + query);
            return query;
        } else if (operation == Operation.INSERT) {
            LOG.debug("Operation 'INSERT' was selected.");
            query = String.format(BASE_INSERT, tableName.getValue(), arguments.get(0), arguments.get(1));
            LOG.debug("Query: " + query);
            return query;
        } else if (operation == Operation.DELETE) {
            LOG.debug("Operation 'DELETE' was selected.");
            query = String.format(BASE_DELETE, tableName.getValue(), arguments.get(0));
            LOG.debug("Query: " + query);
            return query;
        }
        LOG.debug("Operation 'UPDATE' was selected.");
        query = String.format(BASE_UPDATE, tableName.getValue(), arguments.get(0), arguments.get(1), arguments.get(2));
        LOG.debug("Query: " + query);
        return query;
    }
}
