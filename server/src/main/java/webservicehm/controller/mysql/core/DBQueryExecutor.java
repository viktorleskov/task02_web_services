package webservicehm.controller.mysql.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBQueryExecutor {
    private static Logger LOG = LogManager.getLogger(DBQueryExecutor.class.getName());

    private DBQueryExecutor() {
    }

    public static ResultSet executeSelect(String sqlQuery) throws SQLException {
        Connection conn = DBClient.getConnection();
        Statement stmt = conn.createStatement();
        LOG.debug("trying to execute query(SELECT operation)");
        return stmt.executeQuery(sqlQuery);
    }

    public static void execute(String sqlQuery) throws SQLException {
        Connection conn = DBClient.getConnection();
        Statement stmt = conn.createStatement();
        LOG.debug("trying to execute query");
        stmt.execute(sqlQuery);
    }

}
