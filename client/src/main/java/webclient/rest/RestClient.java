package webclient.rest;

import com.google.gson.Gson;
import org.apache.http.util.EntityUtils;
import webclient.dto.User;
import webclient.http.HttpCrudClient;

import java.io.IOException;

public class RestClient {
    private static final String base_url = "http://localhost:8080/swagger/api/user/";


    public static User getUser(String email) {
        String resultJson = "";
        try {
            resultJson = EntityUtils.toString(HttpCrudClient.executeGet(base_url + "get/" + email).getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Gson().fromJson(resultJson, User.class);
    }

    public static void deleteUser(String email) {
        HttpCrudClient.executeDelete(base_url + "delete/" + email);
    }

}
