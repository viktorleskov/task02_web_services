package webservicehm.configuration.mysqlconfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static webservicehm.configuration.mysqlconfig.Property.*;

public class DBProperties {
    private Properties props = new Properties();

    public DBProperties() {
        try {
            props.load(new FileInputStream("src/main/resources/db_settings.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getDbUser() {
        return props.getProperty(DB_USERNAME.prop());
    }

    public String getDbPass() {
        return props.getProperty(DB_USER_PASSWORD.prop());
    }

    public String getDbName() {
        return props.getProperty(DB_NAME.prop());
    }

    public String getDbHost() {
        return props.getProperty(DB_HOST.prop());
    }

    public int getDbPort() {
        return Integer.parseInt(props.getProperty(DB_PORT.prop()));
    }

    public String getJDBCUrl() {
        return String.format(props.getProperty(JDBC_URL.prop()), getDbHost(), getDbPort(), getDbName());
    }
}
