package webservicehm.controller.mysql.utils;

public enum Entity {
    USER("user");

    private String value;

    public String getValue() {
        return value;
    }

    Entity(String value) {
        this.value = value;
    }
}
