package webservicehm.controller.rest.utils;

import com.google.gson.Gson;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class ResultSetParser {
    public static Object parseResultSet(ResultSet resultSet, Class clazz) throws IllegalAccessException, InstantiationException, SQLException {
        return new Gson().fromJson(ResultSetToJsonConverter.convert(resultSet), clazz);
    }
}
